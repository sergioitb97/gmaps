package cat.itb.maps_app.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import cat.itb.maps_app.R;
import cat.itb.maps_app.fragment.Fragment1;
import cat.itb.maps_app.fragment.MapsFragment;

import static cat.itb.maps_app.R.menu.menu;

public class MainActivity extends AppCompatActivity {

    Fragment currentFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //inflar el buen menu
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //cargar un fragment ootro dentro del container
        switch (item.getItemId()) {
            case R.id.menuFragment1:
                currentFragment = new Fragment1();
                break;
            case R.id.menuMap:
                currentFragment = new MapsFragment();
                break;
        }

        changeFragment(currentFragment);
        return super.onOptionsItemSelected(item);
    }

    private void changeFragment(Fragment currentFragment) {

        //Loading the fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, currentFragment).commit();

    }


}